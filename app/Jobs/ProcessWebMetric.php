<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\Gig;

class ProcessWebMetric implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $gig;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Gig $gig)
    {
        $this->gig = $gig;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stop = false;

        while (!$stop) {
            $metricUrl = 'https://seo-rank.my-addr.com/api2/alexa+moz+fb+sr/8992BE363B1A9F7575CD35B7FACC54B9/'.($this->gig->website_url);
            $client = new Client();
            $result = $client->get($metricUrl);
            $metricResults = json_decode((String) $result->getBody());

            if($metricResults->da != "unknown") {
                $this->gig->da = floatval($metricResults->da);
                $this->gig->pa = floatval($metricResults->pa);
                $this->gig->mozrank = floatval($metricResults->mozrank);
                $this->gig->links = floatval($metricResults->links);
                $this->gig->equity = floatval($metricResults->equity);
                $this->gig->alexa_rank = intval($metricResults->a_rank);
                $this->gig->alexa_links = floatval($metricResults->a_links);
                $this->gig->alexa_country = floatval($metricResults->a_cnt);
                $this->gig->alexa_country_rank = floatval($metricResults->a_cnt_r);
                $this->gig->sr_rank = floatval($metricResults->sr_rank);
                $this->gig->sr_keywords = floatval($metricResults->sr_kwords);
                $this->gig->sr_traffic = floatval($metricResults->sr_traffic);
                $this->gig->sr_costs = floatval($metricResults->sr_costs);
                $this->gig->sr_ulinks = floatval($metricResults->sr_ulinks);
                $this->gig->sr_hlinks = floatval($metricResults->sr_hlinks);
                $this->gig->sr_dlinks = floatval($metricResults->sr_dlinks);
                $this->gig->fb_comments = floatval($metricResults->fb_comments);
                $this->gig->fb_shares = floatval($metricResults->fb_shares);
                $this->gig->fb_reacts = floatval($metricResults->fb_reac);
                $this->gig->save();

                $stop = true;
            } else {
                $stop = false;

                sleep(10);
            }

        }

    }
}
