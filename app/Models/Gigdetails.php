<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Gigdetails extends Model
{
    use Sortable;
    //
    public $sortable = ['id', 'min_word', 'days_fulfillment', 'created_at'];
    
    public function Gig(){
        return $this->belongsTo('App\Models\Gig');
    }
    
}
