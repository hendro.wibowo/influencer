<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use DB;
use Session;
use DataTables;

use App\Models\Gig;
use App\Models\Review;
use App\Models\User;
use App\Models\Myorder;

class HomesController extends Controller {


    public function index(){
       $pageTitle = 'Welcome';
       $gigcatlist = Gig::where(['status'=>1])->orderBy('id', 'ASC')->limit(15)->get();
       $mysavegigs = $this->getSavedGigs();
       $recentCompletedlist  = Myorder::with('Gig')->whereHas('Gig', function($q){$q->where('title', '!=', ''); })->where('status', 2)->orderBy('id', 'ASC')->limit(16)->get();
       if(Session::get('user_id')){
            $loginuser = User::where(['id'=>Session::get('user_id')])->first();
            return view('homes.loginindex', ['title' => $pageTitle, 'loginuser'=>$loginuser, 'gigcatlist'=>$gigcatlist, 'mysavegigs'=>$mysavegigs, 'recentCompletedlist'=>$recentCompletedlist]);
        }else{
            $testimonils = DB::table('testimonials')->where('status', 1)->orderBy('id', 'DESC')->limit(6)->get();
            return view('homes.index', ['title' => $pageTitle, 'fixheader'=>1, 'gigcatlist'=>$gigcatlist, 'mysavegigs'=>$mysavegigs, 'recentCompletedlist'=>$recentCompletedlist, 'testimonils'=>$testimonils]);
        }

    }
    public function home(){
        $pageTitle = 'Welcome';

    }

    public function categories(){
       $pageTitle = 'Expore Jobs by Categories';
       $categories = DB::table('categories')->where(['status'=>1, 'parent_id'=>0])->get();
       return view('homes.categories', ['title' => $pageTitle, 'categories'=>$categories]);
    }


    public function sendmail(){
       $uname = array('uname' => 'dinesh');
       Mail::send('emails.welcome', $uname, function($message) use ($uname)
        {
            $message->setSender(array('dinesh.dhaker@logicspice.com' => 'Demo'));
            $message->setFrom(array('dinesh.dhaker@logicspice.com' => 'Demo'));
            $message->to('dinesh.dhaker@logicspice.com', 'John Smith')->subject('Welcome!');
        });
        $email_address = 'dinesh.dhaker@logicspice.com';
         if (count(Mail::failures()) > 0) {
                    echo $errors = 'Failed to send password reset email, please try again.';
                    foreach (Mail::failures() as $email_address) {
                        echo " - $email_address <br />";
                    }
                }
        echo 'ff';
    }

    public function fetchGigData(Request $request, $isHome = 0)
    {
        $input = $request->all();

        $gigs = Gig::with(['Category', 'Language'])->where(function($query) use ($input) {
            if (isset($input['filterKeyword'])) {
                if($input['filterKeyword']) {
                    $query->where('website_url', 'like', '%'.$input['filterKeyword'] . '%');
                }
            }
        });

        if (isset($input['filterCategory'])) {
            if ($input['filterCategory']) {
                $gigs->whereHas('Category', function ($query) use ($input) {
                    $query->where('id', $input['filterCategory']);
                });
            }
        }

        if (isset($input['filterLanguage'])) {
            if ($input['filterLanguage']) {
                $gigs->whereHas('Language', function ($query) use ($input) {
                    $query->where('id', $input['filterLanguage']);
                });
            }
        }

        if ($isHome) {
            $gigs = $gigs->where('status', 1)->take(5);
            return DataTables::of($gigs)
                ->addColumn('channel', function($gig){
                    $categories = $gig->Category;
                    // $languages = $gig->Language;

                    $html  = '<p class="mb-1"><a href="'.$gig->channel_url.'">'.str_replace(['https://', 'https://'], '', $gig->channel_url).'</a></p>';
                    $html .= '<p class="mb-1">Type: <a href="'.$gig->channel_type.'">'.str_replace(['https://', 'https://'], '', $gig->channel_type).'</a></p>';
                    $html .= '<p class="m-0">Kategori: ';

                    foreach ($categories as $cat) {
                        $html .= '<span class="border" style="padding: 4px;
                            margin-right: 5px;
                            border: 1px #cec8c8 solid;
                            border-radius: 4px;
                            background-color: #ececec;">'.$cat->name.'</span>';
                    }

                    $html .= '</p>';

                    return $html;
                })
                ->editColumn('price', '{{number_format($price)}}')
                ->addColumn('action', function($gig) {
                    return '<a data-toggle="modal" class="bl-details" href="#" data-url="/gig-details/'.$gig->slug.'/ajax" data-orderurl="/gig-details/'.$gig->slug.'" data-target="#detail-backlink">Detail</a>';
                })
                ->rawColumns(['channel', 'action'])
                ->orderColumn('channel_url', 'channel_url $1')->make(true);
        } else {
            $user_id = Session::get('user_id');

            if ($user_id) {
                $gigs = $gigs->where('user_id', $user_id);
            } else {
                $gigs = $gigs->where('status', 1);
            }

            return DataTables::of($gigs)
                ->addColumn('channel', function($gig){
                    $categories = $gig->Category;
                    // $languages = $gig->Language;

                    $html  = '<p class="mb-1"><a href="'.$gig->channel_url.'">'.str_replace(['https://', 'https://'], '', $gig->channel_url).'</a></p>';
                    $html .= '<p class="mb-1">Type: <a href="'.$gig->channel_type.'">'.str_replace(['https://', 'https://'], '', $gig->channel_type).'</a></p>';
                    $html .= '<p class="m-0">Kategori: ';

                    foreach ($categories as $cat) {
                        $html .= '<span class="border" style="padding: 4px;
                            margin-right: 5px;
                            border: 1px #cec8c8 solid;
                            border-radius: 4px;
                            background-color: #ececec;">'.$cat->name.'</span>';
                    }

                    $html .= '</p>';

                    return $html;
                })
                ->editColumn('price', '{{number_format($price)}}')
                ->editColumn('status', '{{$status?"PUBLISHED":"DRAFT"}}')
                ->addColumn('action', function($gig) use ($user_id) {
                    if (!$user_id) {
                        return '<a data-toggle="modal" class="bl-details" href="#" data-url="/gig-details/'.$gig->slug.'/ajax" data-orderurl="/gig-details/'.$gig->slug.'" data-target="#detail-backlink">Detail</a>';
                    } else {
                        return '<div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                            <li><a href="' . \URL::to( 'gigs/edit/'.$gig->slug). '" class=""><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;<span>Edit</span></a></li>
                            <li><a href="' . \URL::to( 'gig-details/'.$gig->slug). '" class=""><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp;<span>View Detail</span></li>
                            <li><a href="' . \URL::to( 'gigs/delete/'.$gig->slug). '" onclick="return confirm(\'Are you sure you want to delete this record?\')"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;<span>Delete</span></li>
                            </ul>
                        </div>';
                    }
                })
                ->rawColumns(['channel', 'action'])
                ->orderColumn('channel_url', 'channel_url $1')->make(true);
        }
    }

    public function detailAjax(Request $request, $slug = null) {
        $gigData = Gig::where('slug', $slug)->first();

        if (empty($gigData)) {
            return Redirect::to('gigs/management');
        }

        $userInfo = array();
        if(isset($gigData->User->slug)){
            $userInfo = User::where('slug', $gigData->User->slug)->first();
        }

        $query = new Review();
        $query = $query->with('Myorder');
        $query = $query->where('status', 1);

        $gig_id = $gigData->id;
        $query = $query->whereHas('Myorder', function($q) use ($gig_id){
            $q->where('gig_id', $gig_id)->where('as_a', 'seller');
        });

        $gigreviews  = $query->orderBy('id', 'DESC')->limit(10)->get();

        $date1 = date('Y-m-d',strtotime("-30 days"));
        $sellingOrders = DB::table('myorders')
                ->select('seller_id', 'id', DB::raw('sum(total_amount) as total_sum'))
                ->where('seller_id','=', Session::get('user_id'))
                ->where('created_at','>=', $date1)
                ->get();

        $topRatedInfo = DB::table('reviews')->where(['otheruser_id'=>Session::get('user_id')])->where('rating','>',4)->pluck(DB::raw('count(*) as total'),'id')->all();

        return view('gigs.detail-ajax', [ 'recordInfo' => $gigData, 'userInfo' => $userInfo, 'topRatedInfo' =>$topRatedInfo,'sellingOrders' => $sellingOrders, 'gigreviews'=>$gigreviews]);
    }
}
?>
