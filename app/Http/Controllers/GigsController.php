<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Redirect;
use Input;
use Validator;
use DB;
use Mail;
use App\Mail\SendMailable;
use App\Models\Gig;
use App\Models\User;
use App\Models\Gigdetails;
use App\Models\Gigownership;
use App\Models\Image;
use App\Models\Category;
use App\Models\Skill;
use App\Models\Review;
use App\Models\Language;
use App\Jobs\RunCrawlingJob;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\Remote\DesiredCapabilities;

class GigsController extends Controller {

    public function __construct() {
        $this->middleware('is_userlogin', ['except' => ['listing', 'detail', 'saveChannelMetricData']]);
    }

    public function create() {
        $pageTitle = 'Create a new Gig';
        $skills = Skill::getSkillList();
        $languages = Language::getLanguageLists();
        $input = Input::all();

        $user_id = Session::get('user_id');
        if (!empty($input)) {
            $rules = array(
                'channel_type' => 'required',
                'channel_url' => 'required|min:5|max:80|url',
                'categories' => 'required',
                'languages' => 'required'
            );
            $customMessages = [
                'categories.required' => 'The category field is required field.',
                'language.required' => 'The language field is required field.'
            ];
            $validator = Validator::make($input, $rules, $customMessages);
            if ($validator->fails()) {
                return Redirect::to('gigs/create')->withErrors($validator)->withInput();
            } else {

                $serialisedData = $this->serialiseFormData($input);

                $categoriesData = $serialisedData['categories'];
                $languagesData = $serialisedData['languages'];

                unset($serialisedData['stepcnt']);

                $url = str_replace(['https://', 'http://', '//', '.'], ['', '', '', '-'], $input['channel_url']);
                $slug = $this->createSlug($url, 'gigs');

                $serialisedData['slug'] = $slug;
                $serialisedData['user_id'] = $user_id;
                $serialisedData['current_step'] = 2;

                unset($serialisedData['categories']);
                unset($serialisedData['languages']);

                $gigId = Gig::insertGetId($serialisedData);

                if (!empty(array_filter($categoriesData))) {
                    DB::table('gig_skill')->where('gig_id', $gigId)->delete();
                    foreach ($categoriesData as $skill) {
                        $gigSkill = [
                            'gig_id' => $gigId,
                            'skill_id' => $skill,
                        ];

                        DB::table('gig_skill')->insert($gigSkill);
                    }
                }

                if (!empty(array_filter($languagesData))) {
                    DB::table('gig_language')->where('gig_id', $gigId)->delete();
                    foreach ($languagesData as $lang) {
                        $gigLang = [
                            'gig_id' => $gigId,
                            'lang_id' => $lang,
                        ];

                        DB::table('gig_language')->insert($gigLang);
                    }
                }

                Session::flash('success_message', "Gig details saved successfully.");
                return Redirect::to('gigs/edit/' . $slug);
            }
        }

        return view('gigs.create', ['title' => $pageTitle, 'skills' => $skills, 'languages' => $languages]);
    }

    public function edit($slug = null) {
        $pageTitle = 'Edit Gig';
        $skills = Skill::getSkillList();
        $languages = Language::getLanguageLists();

        $gigData = Gig::where('slug', $slug)->first();
        if (empty($gigData)) {
            return Redirect::to('users/dashboard');
        }


        $gigDetails = Gigdetails::where('gig_id', $gigData->id)->first();
        $gigOwnership = Gigownership::where('gig_id', $gigData->id)->first();

        $input = Input::all();
        $user_id = Session::get('user_id');
        if (!empty($input)) {
            // dd($input);
            if ($input['stepcnt'] == 1) {
                $rules = array(
                    'channel_type' => 'required',
                    'channel_url' => 'required|min:5|max:80|url',
                    'categories' => 'required',
                    'languages' => 'required'
                );
                $customMessages = [
                    'categories.required' => 'The category field is required field.',
                    'language.required' => 'The language field is required field.'
                ];

                $validator = Validator::make($input, $rules, $customMessages);
            } else {
                if ($input['stepcnt'] == 2) {
                    $rules = array(
                        'promotion' => 'required',
                        'price' => 'required|numeric',
                        'days_fulfillment' => 'required|integer',
                    );
                } else {
                    $rules = array();
                }

                $validator = Validator::make($input, $rules);
            }

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            } else {
                $skillsArray = array();
                if (isset($input['categories'])) {
                    foreach ($input['categories'] as $skilList) {
                        $skillinfo = Skill::where('name', $skilList)->first();

                        if ($skillinfo && $skillinfo->id) {
                            $skillsArray[] = $skillinfo->id;
                        } else {
                            $serialisedSkillData = array();
                            $serialisedSkillData['name'] = $skilList;
                            $serialisedSkillData['status'] = 0;
                            $serialisedSkillData['user_id'] = $user_id;
                            $serialisedSkillData['slug'] = $this->createSlug($skilList, 'skills');
                            $skillsArray[] = Skill::insertGetId($serialisedSkillData);
                        }
                    }
                }

                $langArray = array();
                if(isset($input['languages'])) {
                    foreach($input['languages'] as $langCode) {
                        $langInfo = Language::where('code', $langCode)->first();

                        if ($langInfo && isset($langInfo->id)) {
                            $langArray[] = $langInfo->id;
                        }
                    }
                }

                $serialisedData = $this->serialiseFormData($input);

                if (!empty(array_filter($skillsArray))) {
                    DB::table('gig_skill')->where('gig_id', $gigData->id)->delete();
                    foreach ($skillsArray as $skill) {
                        $gigSkill = [
                            'gig_id' => $gigData->id,
                            'skill_id' => $skill,
                        ];

                        DB::table('gig_skill')->insert($gigSkill);
                    }
                }

                if (!empty(array_filter($langArray))) {
                    DB::table('gig_language')->where('gig_id', $gigData->id)->delete();
                    foreach ($langArray as $langId) {
                        $langSkill = [
                            'gig_id' => $gigData->id,
                            'lang_id' => $langId,
                        ];
                        DB::table('gig_language')->insert($langSkill);
                    }
                }

                if ($serialisedData['stepcnt'] == 4) {
                    $serialisedData['status'] = 1;
                }

                $currentstepcount = $serialisedData['stepcnt'];

                unset($serialisedData['stepcnt']);
                unset($serialisedData['categories']);
                unset($serialisedData['languages']);

                switch ($currentstepcount) {
                    case 1:
                        unset($serialisedData['promotion']);
                        unset($serialisedData['price']);
                        unset($serialisedData['days_fulfillment']);

                        $serialisedData['current_step'] = $currentstepcount;

                        Gig::where('id', $gigData->id)->update($serialisedData);

                        return response()->json(['errors' => '', 'status' => 1, 'message' => ["Listing overview data was saved!"], 'gigslug' => [$gigData->slug]]);
                        break;

                    case 2:
                        unset($serialisedData['channel_type']);
                        unset($serialisedData['channel_url']);
                        unset($serialisedData['categories']);
                        unset($serialisedData['languages']);

                        $serialisedData['gig_id'] = $gigData->id;
                        $serialisedData['user_id'] = $gigData->user_id;

                        $gigData->price = $serialisedData['price'];
                        $gigData->days_fulfillment = $serialisedData['days_fulfillment'];

                        foreach ($serialisedData['promotion'] as $promotion => $pdata) {
                            if (isset($pdata['type']) && isset($pdata['price'])) {
                                $gigData->{"promotion_".$promotion} = $pdata['type'];
                                $gigData->{"promotion_".$promotion."_price"} = $pdata['price'];
                            }
                        }

                        $gigData->current_step = $currentstepcount + 1;
                        $gigData->save();

                        return response()->json(['errors' => '', 'status' => 1, 'message' => ["Detail was saved!"], 'gigslug' => [$gigData->slug]]);
                        break;

                    case 3:
                        $ownership = Gigownership::where('gig_id', $gigData->id)->first();
                        $verificationCode = $ownership->code;

                        Storage::put('public/verifications/'.$verificationCode.'.txt', $verificationCode);

                        try {
                            $testUrl = (config('app.env') == 'local' ? config('app.url') . '/public/storage/verifications' : $gigData->channel_url).'/'.$ownership->code.'.txt';
                            $client = new Client();
                            $result = $client->get($testUrl);
                            $body = (String) $result->getBody();

                            if ($body === $ownership->code) {
                                $ownership->verified = true;
                                $ownership->save();
                            } else {

                            }

                            Gig::where('id', $gigData->id)->update(['current_step' => $currentstepcount + 1]);

                            return response()->json(['errors' => '', 'status' => 1, 'message' => ["Successfully verify your website!"], 'gigslug' => [$gigData->slug]]);
                        } catch (GuzzleException $ex) {
                            return response()->json(['errors' => ['Can\'t verify your website using available methods!']]);
                        }

                        break;

                    default:
                        Gig::where('id', $gigData->id)->update(['status' => 1]);

                        ProcessWebMetric::dispatch($gigData);
                        ProcessWebMetricMajestic::dispatch($gigData);

                        Session::flash('success_message', "Your listing was saved and published successfully.");

                        // return Redirect::to('gigs/' . $gigData->slug);
                        break;
                }
            }
        }

        return view('gigs.edit', [
            'title' => $pageTitle,
            'skills' => $skills,
            'languages' => $languages,
            'gigOverviewData' => $gigData,
            'gigOwnership' => $gigOwnership,
        ]);
    }

    public function verificationTxtFile($slug) {
        try {
            $baclink = Gig::where('slug', $slug)->first();
            $verificationCode = $baclink->Gigownership->code;
            return response()->download('storage/app/public/verifications/'.$verificationCode.'.txt');
        } catch (ModelNotFoundException $e) {
            return 'No verification file found.';
        }
    }

    public function add() {
        $pageTitle = 'Manage Settings';
        return view('gigs.add', ['title' => $pageTitle]);
    }

    public function management(Request $request) {
        $pageTitle = 'Manage Settings';
        $query = new Gig();

        $query = $query->where('user_id', '=', Session::get('user_id'));
        $query = $query->whereNull('type_gig');
//        if ($request->has('category_id') && $request->get('category_id') > 0) {
//            $query = $query->where('category_id', $request->get('category_id'));
//        }
        if ($request->has('page')) {
            $page = $request->get('page');
        } else {
            $page = 1;
        }
        if ($page == 1) {
            $limit = 19;
        } else {
            $limit = 20;
        }
        $allrecords = $query->orderBy('id', 'DESC')->paginate($limit, ['*'], 'page', $page);
        if ($request->ajax()) {
            return view('elements.gigs.management', ['allrecords' => $allrecords, 'page' => $page]);
        }
        //echo "<pre>"; print_r($allrecords);exit;
        $catList = Category::getCategoryList();
        return view('gigs.management', ['title' => $pageTitle, 'allrecords' => $allrecords, 'catList' => $catList, 'page' => $page, 'limit' => $limit]);
    }

    public function uploaddocument() {
        $msgString = "";
        $input = Input::all();
        $user_id = Session::get('user_id');
        if (!empty($input)) {
//            echo "<pre>"; print_r($input);exit;
            $rules = array(
                'files_name' => 'mimes:doc,docx,pdf',
            );

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {

                return response()->json(['errors' => $validator->errors()->all()]);
                //return Redirect::to('/admin/gigs/create')->withErrors($validator)->withInput();
            } else {

                $files = explode(',', $input['pdf_doc']);
                if (Input::hasFile('files_name')) {
                    $file = Input::file('files_name');
                    $uploadedFileName = $this->uploadImage($file, GIG_DOC_FULL_UPLOAD_PATH);
                    $rand = rand(100, 999);
                    $html = '<li id="' . $rand . '" data-img="' . $uploadedFileName . '" class="portfolio-cc">' . $uploadedFileName . '<a href="#" onclick="deletefile(' . $rand . ')" class="delete"><i class="fa fa-trash-o"></i></a></li>';
                    $files[] = $uploadedFileName;
                }
                return response()->json(['errors' => '', 'status' => 1, 'message' => ["Gig document is successfully uploaded."], 'file_name' => [$html], 'json_data' => [implode(',', $files)]]);
            }
        }
        exit;
    }

    public function delete($slug = null) {
        if ($slug) {
            Gig::where('slug', $slug)->delete();
            Session::flash('success_message', "Gig deleted successfully.");
            return Redirect::to('gigs/management');
        }
    }

    public function deleteimage($id = null) {
        if ($id) {
            Image::where('id', $id)->delete();
            exit;
        }
    }

    public function listing(Request $request, $catslug = null, $subcatslug = null) {
        $pageTitle = 'View Gigs';

        $query = new Gig();
        $query = $query->with('User');
        $query = $query->where('status', 1);

        if ($request->has('page')) {
            $page = $request->get('page');
        } else {
            $page = 1;
        }

        $limit = 16;

        $allrecords = $query->paginate($limit, ['*'], 'page', $page);

        if ($request->ajax()) {
            return view('elements.gigs.listing', ['allrecords' => $allrecords, 'page' => $page, 'isajax'=>1]);
        }

        $countryLists  = DB::table('countries')->where('status', 1)->orderBy('name', 'ASC')->pluck('name','id')->all();

        return view('gigs.listing', [
            'title' => $pageTitle,
            'allrecords' => $allrecords,
            'page' => $page,
            'limit' => $limit,
            'countryLists'=>$countryLists,
            'skills' => Skill::where('status', 1)->orderBy('name')->get(),
            'languages' => Language::orderBy('name')->get(),
        ]);
    }

    public function offeredgig() {
        $pageTitle = 'Offered Gigs';


        $allrecords = Gig::where('offer_user', Session::get('user_id'))->orderBy('id', 'DESC')->get();

        return view('gigs.offeredgig', ['title' => $pageTitle, 'allrecords' => $allrecords]);
    }

    public function myofferedgig() {
        $pageTitle = 'My Offered Gigs';

        $allrecords = Gig::where('user_id', Session::get('user_id'))->where('type_gig', 'offer')->orderBy('id', 'DESC')->get();

        return view('gigs.myofferedgig', ['title' => $pageTitle, 'allrecords' => $allrecords]);
    }

    public function detail(Request $request, $slug = null) {
        $pageTitle = 'View Gig Detail';

        $gigData = Gig::where('slug', $slug)->first();


//        echo '<pre>';print_r($gigData);exit;
        if (empty($gigData)) {
            return Redirect::to('gigs/management');
        }
        $userInfo = array();
        if(isset($gigData->User->slug)){
            $userInfo = User::where('slug', $gigData->User->slug)->first();
        }

        $pageTitle = $gigData->title;

        $query = new Review();
        $query = $query->with('Myorder');
        $query = $query->where('status', 1);

        $gig_id = $gigData->id;
        $query = $query->whereHas('Myorder', function($q) use ($gig_id){
            $q->where('gig_id', $gig_id)->where('as_a', 'seller');
        });

        $gigreviews  = $query->orderBy('id', 'DESC')->limit(10)->get();

        $date1 = date('Y-m-d',strtotime("-30 days"));
        $sellingOrders = DB::table('myorders')
                ->select('seller_id', 'id', DB::raw('sum(total_amount) as total_sum'))
                ->where('seller_id','=', Session::get('user_id'))
                ->where('created_at','>=', $date1)
                ->get();

        $topRatedInfo = DB::table('reviews')->where(['otheruser_id'=>Session::get('user_id')])->where('rating','>',4)->pluck(DB::raw('count(*) as total'),'id')->all();

        return view('gigs.detail', ['title' => $pageTitle, 'recordInfo' => $gigData, 'userInfo' => $userInfo, 'topRatedInfo'=>$topRatedInfo,'sellingOrders' => $sellingOrders, 'gigreviews'=>$gigreviews]);
    }

    public function createoffer(Request $request){
        $pageTitle = 'Create a new Gig';
        $catList = Category::getCategoryList();
        $skills = Skill::getSkillList();
        $input = Input::all();

        $user_id = Session::get('user_id');
        if (!empty($input)) {

            $gigData = Gig::where('id', $input['select_gig'])->first();

            $serialisedData['id'] = '';
            $serialisedData['basic_description'] = $input['description'];
            $serialisedData['basic_price'] = $input['basic_price'];
            $serialisedData['basic_delivery'] = $input['basic_delivery'];
            $serialisedData['expiry'] = $input['expiry'];
            $serialisedData['one_delivery'] = 1;
            $serialisedData['standard_title'] = '';
            $serialisedData['standard_description'] = '';
            $serialisedData['standard_delivery'] = '';
            $serialisedData['standard_revision'] = '';
            $serialisedData['standard_price'] = '';
            $serialisedData['premium_title'] = '';
            $serialisedData['premium_description'] = '';
            $serialisedData['premium_delivery'] = '';
            $serialisedData['premium_revision'] = '';
            $serialisedData['premium_price'] = '';
            $serialisedData['title'] = $gigData->title;
            $serialisedData['category_id'] = $gigData->category_id;
            $serialisedData['subcategory_id'] = $gigData->subcategory_id;
            $serialisedData['tags'] = $gigData->tags;
            $serialisedData['description'] = $gigData->description;
            $serialisedData['photo'] = $gigData->photo;
            $serialisedData['youtube_url'] = $gigData->youtube_url;
            $serialisedData['youtube_image'] = $gigData->youtube_image;
            $serialisedData['pdf_doc'] = $gigData->pdf_doc;

            $slug = $this->createSlug($gigData->title, 'gigs');
            $serialisedData['slug'] = $slug;
            $serialisedData['user_id'] = $user_id;
            $serialisedData['type_gig'] = 'offer';
            $serialisedData['offer_user'] = $input['offer_user'];

            $userInfo = User::where('id', $input['offer_user'])->first();
            Gig::insert($serialisedData);

            $gigId = DB::getPdo()->lastInsertId();

            if($gigData->Image){
                foreach($gigData->Image as $gigimage) {
                    if (isset($gigimage->name) && !empty($gigimage->name)){
                        $path = GIG_FULL_UPLOAD_PATH . $gigimage->name;
                        if (file_exists($path) && !empty($gigimage->name)){
                            $uploadedFileName = $gigimage->name;
                            $uploadedFileNew = $gigimage->name.'-'.time();
                            $success = \File::copy(GIG_FULL_UPLOAD_PATH.'/'.$uploadedFileName,GIG_FULL_UPLOAD_PATH.'/'.$uploadedFileNew);

                            $this->resizeImage($uploadedFileNew, GIG_FULL_UPLOAD_PATH, GIG_SMALL_UPLOAD_PATH, GIG_MW, GIG_MH);

                            $serialisedImgData = array();

                            $serialisedImgData['gig_id'] = $gigId;
                            $serialisedImgData['name'] = $uploadedFileName;
                            $serialisedImgData['status'] = 1;
                            $serialisedImgData['main'] = 0;

                            Image::insert($serialisedImgData);
                        }
                    }
                }
            }

            $name = ucwords($userInfo->first_name . ' ' . $userInfo->last_name);
            $username = ucwords($gigData->User->first_name . ' ' . $gigData->User->last_name);
            $price = '$'.$input['basic_price'];
            $duedate = date('d M Y',strtotime($input['expiry']));
            $item = $gigData->title;
            $emailId = $userInfo->email_address;

            $emailTemplate = DB::table('emailtemplates')->where('id', 22)->first();
            $toRepArray = array('[!username!]', '[!name!]', '[!duedate!]', '[!item!]', '[!price!]', '[!HTTP_PATH!]', '[!SITE_TITLE!]');
            $fromRepArray = array($username, $name, $duedate, $item, $price, HTTP_PATH, SITE_TITLE);
            $emailSubject = str_replace($toRepArray, $fromRepArray, $emailTemplate->subject);
            $emailBody = str_replace($toRepArray, $fromRepArray, $emailTemplate->template);
            Mail::to($emailId)->send(new SendMailable($emailBody, $emailSubject));

            Session::flash('success_message', "Gig details saved successfully.");
            return Redirect::to('gigs/myofferedgig');
        }

    }

    public function acceptreject(Request $request, $type, $slug) {
        if($type == 1){
            $pageTitle = 'Accept Offer';
            $gigData = Gig::where('slug', $slug)->first();
            $userInfo = User::where('id', $gigData->offer_user)->first();

            DB::table('gigs')->where('id', $gigData->id)->update(array('offer_status' => 1));

            $username = ucwords($userInfo->first_name . ' ' . $userInfo->last_name);
            $name = ucwords($gigData->User->first_name . ' ' . $gigData->User->last_name);
            $price = '$'.$gigData->basic_price;
            $item = $gigData->title;
            $emailId = $gigData->User->email_address;

            $emailTemplate = DB::table('emailtemplates')->where('id', 23)->first();
            $toRepArray = array('[!username!]', '[!name!]', '[!item!]', '[!price!]', '[!HTTP_PATH!]', '[!SITE_TITLE!]');
            $fromRepArray = array($username, $name, $item, $price, HTTP_PATH, SITE_TITLE);
            $emailSubject = str_replace($toRepArray, $fromRepArray, $emailTemplate->subject);
            $emailBody = str_replace($toRepArray, $fromRepArray, $emailTemplate->template);
            Mail::to($emailId)->send(new SendMailable($emailBody, $emailSubject));

            Session::flash('success_message', "Custom offer accepted successfully.");
            return Redirect::to('gigs/offeredgig');

        }elseif($type == 2){
            $pageTitle = 'Reject Offer';
            $gigData = Gig::where('slug', $slug)->first();
            $userInfo = User::where('id', $gigData->offer_user)->first();

            DB::table('gigs')->where('id', $gigData->id)->update(array('offer_status' => 2));

            $username = ucwords($userInfo->first_name . ' ' . $userInfo->last_name);
            $name = ucwords($gigData->User->first_name . ' ' . $gigData->User->last_name);
            $price = '$'.$gigData->basic_price;
            $item = $gigData->title;
            $emailId = $gigData->User->email_address;

            $emailTemplate = DB::table('emailtemplates')->where('id', 24)->first();
            $toRepArray = array('[!username!]', '[!name!]', '[!item!]', '[!price!]', '[!HTTP_PATH!]', '[!SITE_TITLE!]');
            $fromRepArray = array($username, $name, $item, $price, HTTP_PATH, SITE_TITLE);
            $emailSubject = str_replace($toRepArray, $fromRepArray, $emailTemplate->subject);
            $emailBody = str_replace($toRepArray, $fromRepArray, $emailTemplate->template);
            Mail::to($emailId)->send(new SendMailable($emailBody, $emailSubject));

            Session::flash('success_message', "Custom offer rejected successfully.");
            return Redirect::to('gigs/offeredgig');
        }elseif($type == 3){
            $pageTitle = 'Withdrawn Offer';
            $gigData = Gig::where('slug', $slug)->first();
            $userInfo = User::where('id', $gigData->offer_user)->first();

            DB::table('gigs')->where('id', $gigData->id)->update(array('offer_status' => 3));

            $name = ucwords($userInfo->first_name . ' ' . $userInfo->last_name);
            $username = ucwords($gigData->User->first_name . ' ' . $gigData->User->last_name);
            $price = '$'.$gigData->basic_price;
            $item = $gigData->title;
            $emailId = $userInfo->email_address;

            $emailTemplate = DB::table('emailtemplates')->where('id', 25)->first();
            $toRepArray = array('[!username!]', '[!name!]', '[!item!]', '[!price!]', '[!HTTP_PATH!]', '[!SITE_TITLE!]');
            $fromRepArray = array($username, $name, $item, $price, HTTP_PATH, SITE_TITLE);
            $emailSubject = str_replace($toRepArray, $fromRepArray, $emailTemplate->subject);
            $emailBody = str_replace($toRepArray, $fromRepArray, $emailTemplate->template);
            Mail::to($emailId)->send(new SendMailable($emailBody, $emailSubject));

            Session::flash('success_message', "Custom offer rejected successfully.");
            return Redirect::to('gigs/myofferedgig');
        }
    }

    public function crawlChannelUrl($slug)
    {
        $gig = Gig::where('slug', $slug)->first();
        $input = Input::all();

        shell_exec('node scripts/puppet.js');
        // dd('');

        RunCrawlingJob::dispatch($input);

        // if($gig) {
        //     $options = (new ChromeOptions)
        //         ->addArguments([
        //             '--disable-web-security', '--allow-running-insecure-content'
        //         ]);

        //     $browser = RemoteWebDriver::create(
        //         'http://localhost:9515',
        //         DesiredCapabilities::chrome()->setCapability(
        //             ChromeOptions::CAPABILITY,
        //             $options
        //         )
        //     );

        //     // $browser->manage()->timeouts()->implicitlyWait = 5000;

        //     if($gig->channel_type == 'YOUTUBE') {
        //         // ytd-button-renderer/a/paper-button/yt-formatted-string
        //         // $browser->get('https://youtube.com');
        //         $browser->get('https://accounts.google.com/signin/v2/identifier?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3Dhttps%253A%252F%252Fwww.youtube.com%252F&hl=en&ec=65620&flowName=GlifWebSignIn&flowEntry=ServiceLogin');
        //         $browser->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div[role="button"].U26fgb')));
        //         $browser->findElement(WebDriverBy::cssSelector('input[type="email"].whsOnd'))->click();
        //         $browser->getKeyboard()->sendKeys('hendrothemail@gmail.com');
        //         $browser->findElement(WebDriverBy::cssSelector('div[role="button"].U26fgb'))->click();

        //         // $browser->executeScript("arguments[0].click();", [$element]);
        //         // $subscribers = $this->convertTotalStringToNumber(trim(str_replace(' subscribers', '', $subscribersText)));
        //     } else if($gig->channel_type == 'FACEBOOK') {
        //         $subscribersText = $browser->get($gig->channel_url)->findElement(WebDriverBy::id('subscriber-count'))->getText();
        //         $subscribers = $this->convertTotalStringToNumber(trim(str_replace(' subscribers', '', $subscribersText)));
        //     } else if($gig->channel_type == 'INSTAGRAM') {
        //         $subscribersText = $browser->get($gig->channel_url)->findElement(WebDriverBy::id('subscriber-count'))->getText();
        //         $subscribers = $this->convertTotalStringToNumber(trim(str_replace(' subscribers', '', $subscribersText)));
        //     } else {

        //     }

        return response()->json(['errors' => '', 'status' => 1, 'message' => ["Detail was saved!"], 'gigslug' => [$gig->slug]]);
    }

    private function convertTotalStringToNumber($input)
    {
        if($pos = strpos($input, 'M',  -1)) {
            $totalNumber = floatval(substr($input, 0, $pos));
            return $totalNumber * 1000000;
        }

        if($pos = strpos($input, 'K',  -1)) {
            $totalNumber = floatval(substr($input, 0, $pos));
            return $totalNumber * 1000;
        }

        return intval($input);
    }

    public function saveChannelMetricData()
    {
        $inputs = Input::all();
        return response()->json($inputs);
    }
}
