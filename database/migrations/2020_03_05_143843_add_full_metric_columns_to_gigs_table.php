<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullMetricColumnsToGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->float('mozrank')->default(0)->nullable();
            // $table->float('links')->default(0)->nullable();
            $table->float('equity')->default(0)->nullable();
            $table->float('links')->default(0)->nullable();
            $table->float('alexa_links')->default(0)->nullable();
            $table->float('alexa_country')->default(0)->nullable();
            $table->float('alexa_country_rank')->default(0)->nullable();
            $table->float('sr_rank')->default(0)->nullable();
            $table->float('sr_keywords')->default(0)->nullable();
            $table->float('sr_traffic')->default(0)->nullable();
            $table->float('sr_costs')->default(0)->nullable();
            $table->float('sr_ulinks')->default(0)->nullable();
            $table->float('sr_hlinks')->default(0)->nullable();
            $table->float('sr_dlinks')->default(0)->nullable();
            $table->float('fb_comments')->default(0)->nullable();
            $table->float('fb_shares')->default(0)->nullable();
            $table->float('fb_reacts')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->dropColumn('mozrank');
            // $table->dropColumn('links');
            $table->dropColumn('equity');
            $table->dropColumn('links');
            $table->dropColumn('alexa_links');
            $table->dropColumn('alexa_country');
            $table->dropColumn('alexa_country_rank');
            $table->dropColumn('sr_rank');
            $table->dropColumn('sr_keywords');
            $table->dropColumn('sr_traffic');
            $table->dropColumn('sr_costs');
            $table->dropColumn('sr_ulinks');
            $table->dropColumn('sr_hlinks');
            $table->dropColumn('sr_dlinks');
            $table->dropColumn('fb_comments');
            $table->dropColumn('fb_shares');
            $table->dropColumn('fb_reacts');
        });
    }
}
