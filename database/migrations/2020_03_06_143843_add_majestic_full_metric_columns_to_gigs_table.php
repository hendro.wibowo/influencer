<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMajesticFullMetricColumnsToGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->float('ebl')->default(0)->nullable();
            $table->float('ebl_edu')->default(0)->nullable();
            $table->float('ebl_gov')->default(0)->nullable();
            $table->float('refd')->default(0)->nullable();
            $table->float('refd_edu')->default(0)->nullable();
            $table->float('refd_gov')->default(0)->nullable();
            $table->float('ips')->default(0)->nullable();
            $table->float('ccs')->default(0)->nullable();
            $table->float('tf')->default(0)->nullable();
            $table->float('cf')->default(0)->nullable();
            $table->float('ttft0')->default(0)->nullable();
            $table->float('ttfv0')->default(0)->nullable();
            $table->float('ttft1')->default(0)->nullable();
            $table->float('ttfv1')->default(0)->nullable();
            $table->float('ttft2')->default(0)->nullable();
            $table->float('ttfv2')->default(0)->nullable();
            $table->float('ttft3')->default(0)->nullable();
            $table->float('ttfv3')->default(0)->nullable();
            $table->float('ttft4')->default(0)->nullable();
            $table->float('ttfv4')->default(0)->nullable();
            $table->float('ttft5')->default(0)->nullable();
            $table->float('ttfv5')->default(0)->nullable();
            $table->float('ttft6')->default(0)->nullable();
            $table->float('ttfv6')->default(0)->nullable();
            $table->float('ttft7')->default(0)->nullable();
            $table->float('ttfv7')->default(0)->nullable();
            $table->float('ttft8')->default(0)->nullable();
            $table->float('ttfv8')->default(0)->nullable();
            $table->float('ttft9')->default(0)->nullable();
            $table->float('ttfv9')->default(0)->nullable();
            $table->float('domain_status')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->dropColumn('ebl');
            $table->dropColumn('ebl_edu');
            $table->dropColumn('ebl_gov');
            $table->dropColumn('refd');
            $table->dropColumn('refd_edu');
            $table->dropColumn('refd_gov');
            $table->dropColumn('ips');
            $table->dropColumn('ccs');
            $table->dropColumn('tf');
            $table->dropColumn('cf');
            $table->dropColumn('ttft0');
            $table->dropColumn('ttfv0');
            $table->dropColumn('ttft1');
            $table->dropColumn('ttfv1');
            $table->dropColumn('ttft2');
            $table->dropColumn('ttfv2');
            $table->dropColumn('ttft3');
            $table->dropColumn('ttfv3');
            $table->dropColumn('ttft4');
            $table->dropColumn('ttfv4');
            $table->dropColumn('ttft5');
            $table->dropColumn('ttfv5');
            $table->dropColumn('ttft6');
            $table->dropColumn('ttfv6');
            $table->dropColumn('ttft7');
            $table->dropColumn('ttfv7');
            $table->dropColumn('ttft8');
            $table->dropColumn('ttfv8');
            $table->dropColumn('ttft9');
            $table->dropColumn('ttfv9');
            $table->dropColumn('domain_status');
        });
    }
}
