<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetricColumnsToGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->float('da')->default(0)->nullable();
            $table->float('pa')->default(0)->nullable();
            $table->float('google_site')->default(0)->nullable();
            $table->integer('alexa')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->dropColumn('da');
            $table->dropColumn('pa');
            $table->dropColumn('google_site');
            $table->dropColumn('alexa');
        });
    }
}
