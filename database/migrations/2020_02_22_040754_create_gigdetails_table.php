<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gigdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gig_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('min_words')->default(0);
            $table->boolean('article_from_me')->default(false)->nullable();
            $table->integer('additional_price')->default(0)->nullable();
            $table->integer('days_fulfillment')->default(0)->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });

        Schema::create('gigownership', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gig_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('code', 15)->nullable();
            $table->boolean('verified')->default(false);
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gigdetails');
        Schema::dropIfExists('gigownership');
    }
}
