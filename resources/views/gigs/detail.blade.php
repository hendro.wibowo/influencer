@extends('layouts.dashboard')
@section('content')
{{ HTML::script('public/js/front/lightslider.js')}}
<script type="text/javascript">
    var img_path = "{!! HTTP_PATH !!}/public/img";
            $(document).ready(function () {
    $('#image-gallery').lightSlider({
    gallery: true,
            item: 1,
            thumbItem: 9,
            slideMargin: 0,
            speed: 500,
            auto: true,
            loop: true,
            onSliderLoad: function () {
            $('#image-gallery').removeClass('cS-hidden');
            }
    });
    });
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            // 3. This function creates an <iframe> (and YouTube player)
            //    after the API code downloads.
            var player;
            function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
            height: '390',
                    width: '640',
                    videoId: 'M7lc1UVf-VE',
                    events: {
                    'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                    }
            });
            }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
    event.target.playVideo();
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
            function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
                    done = true;
            }
            }
    function stopVideo() {
    player.stopVideo();
    }
</script>
{{ HTML::script('public/js/jquery.raty.min.js')}}
{{ HTML::style('public/css/front/lightslider.css')}}
<div class="main_dashboard">
    @include('elements.topcategories')
    <section class="dashboard-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="top_row_new">
                        <h3 class="left_title">{{ ucfirst(str_replace(['https://', 'http://'], '', rtrim($recordInfo->website_url, '/'))) }}</h3>
                        <hr>

                        <div class="clearfix" id="galleryddd" style="margin-bottom: 3rem">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">URL Domain</div>
                                        <div class="col-md-6">{{ $recordInfo->website_url }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">DA</div>
                                        <div class="col-md-6">{{ $recordInfo->da }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">PA</div>
                                        <div class="col-md-6">{{ $recordInfo->pa }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Mozrank</div>
                                        <div class="col-md-6">{{ $recordInfo->mozrank }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Links</div>
                                        <div class="col-md-6">{{ $recordInfo->links }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Equity</div>
                                        <div class="col-md-6">{{ $recordInfo->equity }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa Rank</div>
                                        <div class="col-md-6">{{ $recordInfo->alexa_rank }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa Links</div>
                                        <div class="col-md-6">{{ $recordInfo->alexa_links }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa CNT</div>
                                        <div class="col-md-6">{{ $recordInfo->alexa_country }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa CNT R</div>
                                        <div class="col-md-6">{{ $recordInfo->alexa_country_rank }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Facebook Comments</div>
                                        <div class="col-md-6">{{ $recordInfo->fb_comments }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Facebook Shares</div>
                                        <div class="col-md-6">{{ $recordInfo->fb_shares }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Facebook Reacts</div>
                                        <div class="col-md-6">{{ $recordInfo->fb_reacts }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Domain</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_domain }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Keywords</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_keywords }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Traffic</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_traffic }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Costs</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_costs }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Ulinks</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_ulinks }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Hlinks</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_hlinks }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Dlinks</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_dlinks }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Dlinks</div>
                                        <div class="col-md-6">{{ $recordInfo->sr_dlinks }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Domain Status</div>
                                        <div class="col-md-6">{{ $recordInfo->domain_status }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">External Backlinks (EBL)</div>
                                        <div class="col-md-6">{{ $recordInfo->ebl }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">External Backlinks EDU</div>
                                        <div class="col-md-6">{{ $recordInfo->ebl_edu }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">External Backlinks GOV</div>
                                        <div class="col-md-6">{{ $recordInfo->ebl_gov }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Referring Domains (RefD)</div>
                                        <div class="col-md-6">{{ $recordInfo->refd }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Referring Domains EDU (RefD_EDU)</div>
                                        <div class="col-md-6">{{ $recordInfo->refd_edu }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Referring Domains GOV (RefD_GOV)</div>
                                        <div class="col-md-6">{{ $recordInfo->refd_gov }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">IP Addresses (IPS)</div>
                                        <div class="col-md-6">{{ $recordInfo->ips }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Class C Subnets (CCS)</div>
                                        <div class="col-md-6">{{ $recordInfo->ccs }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Trust Flow (TF)</div>
                                        <div class="col-md-6">{{ $recordInfo->tf }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Citation Flow (CF)</div>
                                        <div class="col-md-6">{{ $recordInfo->cf }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 0 (TTFT0)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft0 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 0 (TTFV0)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv0 }}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 1 (TTFT1)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft1 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 1 (TTFV1)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv1 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 2 (TTFT2)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft2 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 2 (TTFV2)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv2 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 3 (TTFT3)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft3 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 3 (TTFV3)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv3 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 4 (TTFT4)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft4 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 4 (TTFV4)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv4 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 5 (TTFT5)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft5 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 5 (TTFV5)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv5 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 6 (TTFT6)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft6 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 6 (TTFV6)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv6 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 7 (TTFT7)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft7 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 7 (TTFV7)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv7 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 8 (TTFT8)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft8 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 8 (TTFV8)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv8 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 9 (TTFT9)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttft9 }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 9 (TTFV9)</div>
                                        <div class="col-md-6">{{ $recordInfo->ttfv9 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="about_seller">
                            <h5>About the Seller</h5>
                            <div class="profile-about">
                                <div class="dpimg-about">
                                    @if (file_exists(PROFILE_FULL_UPLOAD_PATH . $recordInfo->User->profile_image) && !empty($recordInfo->User->profile_image))
                                    <a href="{{ URL::to( 'public-profile/'.$recordInfo->User->slug)}}">{{HTML::image(PROFILE_SMALL_DISPLAY_PATH . $recordInfo->User->profile_image, SITE_TITLE)}}</a>
                                    @else
                                    <a href="{{ URL::to( 'public-profile/'.$recordInfo->User->slug)}}">{{HTML::image('public/img/front/dummy.png', SITE_TITLE)}}</a>
                                    @endif
                                </div>
                                <div class="dp_details-about"><h3><a href="{{ URL::to( 'public-profile/'.$recordInfo->User->slug)}}">{{$recordInfo->User->first_name?$recordInfo->User->first_name.' '.$recordInfo->User->last_name:''}} </a></h3>
                                    <p>{{$recordInfo->User->address}}</p>
                                    <div class="about-rating">
                                        <script>
                                        $(function() {
                                            $('#avgRating22').raty({
                                            starOn:    'star-on.png',
                                                    starOff:   'star-off.png',
                                                    start: {{$recordInfo -> User -> average_rating}},
                                                    readOnly: true
                                            });
                                        });
                                        </script>
                                        <span class="pprate gigdtlrat" id="avgRating22"></span>
                                        <span><b><?php echo $recordInfo->User->average_rating; ?></b> (<?php echo $recordInfo->User->total_review; ?> reviews)</span>
                                    </div>
                                    <!--                                <a href="#" class="btn btn-default">Contact Me</a>-->
                                </div>

                                <div class="client-reviews">
                                    <div class="client-reviews-left">
                                        <h3>About me</h3>
                                        <p class="text-viewer"><?php echo $recordInfo->User->description; ?></p>
                                    </div>
                                    <div class="client-reviews-right">
                                        <h3>General Info</h3>
                                        <ul class="general-info">
                                            <li>
                                                <label><i class="fa fa-map-marker" aria-hidden="true"></i>From</label>
                                                <span>
                                                    <?php
                                                    $farray = array();
                                                    if (isset($recordInfo->User->city) && $recordInfo->User->city != '') {
                                                        $farray[] = $recordInfo->User->city;
                                                    }
                                                    if (isset($recordInfo->User->Country->name) && $recordInfo->User->Country->name != '') {
                                                        $farray[] = $recordInfo->User->Country->name;
                                                    }
                                                    echo implode(', ', $farray);
                                                    ?>
                                                </span>
                                            </li>
                                            <li>
                                                <label><i class="fa fa-user" aria-hidden="true"></i>Member since</label>
                                                <span><?php echo date('F Y', strtotime($recordInfo->User->created_at)); ?></span>
                                            </li>
                                            @if($recordInfo->User->languages)
                                            <li>
                                                <label><i class="fa fa-language" aria-hidden="true"></i>Languages</label>
                                                <span>
                                                    @foreach(json_decode($recordInfo->User->languages) as $key => $lang)
                                                    @if(!$loop->first), @endif{!!$lang->lang_name!!}
                                                    @endforeach
                                                </span>
                                            </li>
                                            @endif
                                            <!--                                        <li>
                                                                                        <label><i class="fa fa-clock-o" aria-hidden="true"></i>Avg. Response Time</label>
                                                                                        <span>3 hours</span>
                                                                                    </li>-->
                                            <!--                                        <li>
                                                                                        <label><i class="fa fa-send" aria-hidden="true"></i>Recent Delivery</label>
                                                                                        <span>about 4 hours</span>
                                                                                    </li>-->

                                        </ul>
                                    </div>
                                </div>
                                @if(!$gigreviews->isEmpty())
                                <div class="client-rievews gigdtl_pg">
                                    @foreach($gigreviews as $allrecord)
                                    <div class="client-chat gigdtl_pgin">
                                        <div class="clientimg-about">
                                            @if($allrecord->Otheruser->profile_image)
                                            <a href="{{ URL::to( 'public-profile/'.$allrecord->Otheruser->slug)}}" class="">{{HTML::image(PROFILE_SMALL_DISPLAY_PATH.$allrecord->Otheruser->profile_image, SITE_TITLE)}}</a>
                                            @else
                                            <a href="{{ URL::to( 'public-profile/'.$allrecord->Otheruser->slug)}}" class="">{{HTML::image('public/img/front/user-img.png', SITE_TITLE, ['id'=> 'pimage'])}}</a>
                                            @endif
                                        </div>
                                        <div class="client-rv">
                                            <h3><a href="{{ URL::to( 'public-profile/'.$allrecord->Otheruser->slug)}}" class="">{{$allrecord->Otheruser->first_name.' '.$allrecord->Otheruser->last_name}}</a></h3>
                                            <span class="review-date"><i class="fa fa-calendar" aria-hidden="true"></i>{{$allrecord->created_at->diffForHumans()}}</span>
                                            <div class="client-review-reting">
                                                <script>
                                                            $(function() {
                                                            $('#lst{{$allrecord->id}}').raty({
                                                            starOn:    'star-on.png',
                                                                    starOff:   'star-off.png',
                                                                    start: {{$allrecord -> rating}},
                                                                    readOnly: true
                                                            });
                                                            });</script>
                                                <span class="lstreview lstreview_new" id="lst{{$allrecord->id}}"></span>
                                                <b>{{$allrecord->rating}}</b>
                                            </div>
                                            <p>
                                                {{nl2br($allrecord->comment)}}
                                            </p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-12 col-md-4 sticky">
                    <div class="offer_wrap_top">
{{--
                        <!-- Nav tabs -->
                        <ul class="offer-nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#basic" aria-controls="basic" role="tab" data-toggle="tab" onclick="updateprice('{{$recordInfo->price}}', 'basic')">Basic</a></li>
                        </ul> --}}
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="basic">
                                <div class="offer-bxs">
                                    <h4 style="font-weight: bold">Additional Info</h4><hr>
                                    <div class="offer-bxs-price">
                                        <span class="package-title-text">Min. Words</span>
                                        <span class="package-price">{{$recordInfo->min_words}} words</span>
                                    </div>
                                    <div class="offer-bxs-price">
                                        <span class="package-title-text">Article From Me [?]</span>
                                        <span class="package-price">{{$recordInfo->article_from_me ? 'Yes' : 'No'}} + {{ CURR.number_format($recordInfo->additional_price)}}</span>
                                    </div>
                                    <div class="offer-bxs-price">
                                        <span class="package-title-text">Delivery</span>
                                        <span class="package-price">{{$recordInfo->days_fulfillment}} days</span>
                                    </div>
                                    <div class="offer-bxs-price">
                                        <span class="package-title-text">Price</span>
                                        <span class="package-price">{{CURR.number_format($recordInfo->price)}}</span>
                                    </div>
                                    <p class="package-description">{{$recordInfo->basic_description}}</p>
                                </div>
                            </div>
                            {{ Form::open(array('method' => 'post', 'id' => 'addggiform')) }}
                            <input type="hidden" name="type" id="settype" value="basic">
                            <input type="hidden" name="slug" id="gigslug" value="{{$recordInfo->slug}}">
                            <div class="package-footer">
                                <p class="" id="hidebtn">
                                    @if(Session::get('user_id') != $recordInfo->user_id)
                                    <span  onclick="submitform()" class="btn-lrg-standard" >Order
                                        ({{CURR}}<span class="js-str-currency" id="btnprice">{{number_format(intval($recordInfo->price)+intval($recordInfo->additional_price))}}</span>)
                                    </span>
                                    @endif
                                </p>
                                <div class="gigdloader" id="gigdloader">{{HTML::image("public/img/loading.gif", SITE_TITLE)}}</div>
                            </div>
                            {{ Form::close()}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
<script>
                                    function updateprice(price, ptype){
                                    $('#btnprice').html(price);
                                            $('#settype').val(ptype);
                                    }
//
                            function submitform(){
                            @if (!Session::get('user_id'))
                                    alert('Your must login to place your order.');
                                    @else
                                    $.ajax({
                                    url: "{!! HTTP_PATH !!}/payments/addtocart",
                                            type: "POST",
                                            data: $('#addggiform').serialize(),
                                            //data: { _token: '{{csrf_token()}}'},
                                            beforeSend: function () {$("#gigdloader").show(); $("#hidebtn").hide(); },
                                            success: function (result) {
                                            window.location = "{!! HTTP_PATH !!}/order-summary/" + result;
                                            }
                                    });
                                    @endif
                            }

                            $(function () {
                            $('[data-toggle="tooltip"]').tooltip()
                            });
                                    $("#maraction").click(function () {
                            $("#offer-show").addClass("offer-div");
                                    $(".dashboard-rights-section").removeClass("offer-div");
                            });</script>
<script>
                                    $(function () {
                                    // here the code for text minimiser and maxmiser by faisal khan
                                    var minimized_elements = $('p.text-viewer');
                                            minimized_elements.each(function () {
                                            var t = $(this).text();
                                                    if (t.length < 200)
                                                    return;
                                                    $(this).html(
                                                    t.slice(0, 200) + '<span>... </span><a href="#" class="more"> + See More </a>' +
                                                    '<span style="display:none;">' + t.slice(200, t.length) + ' <a href="#" class="less"> - See Less </a></span>'
                                                    );
                                            });
                                            $('a.more', minimized_elements).click(function (event) {
                                    event.preventDefault();
                                            $(this).hide().prev().hide();
                                            $(this).next().show();
                                    });
                                            $('a.less', minimized_elements).click(function (event) {
                                    event.preventDefault();
                                            $(this).parent().hide().prev().show().prev().show();
                                    });
                                    });</script>
<script>
                                    jQuery(document).ready(function () {
                            function close_accordion_section() {
                            jQuery('.accordion .accordion-section-title').removeClass('active');
                                    jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
                            }

                            jQuery('.accordion-section-title').click(function (e) {
                            // Grab current anchor value
                            var currentAttrValue = jQuery(this).attr('href');
                                    if (jQuery(e.target).is('.active')) {
                            close_accordion_section();
                            } else {
                            close_accordion_section();
                                    // Add active class to section title
                                    jQuery(this).addClass('active');
                                    // Open up the hidden content panel
                                    jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
                            }

                            e.preventDefault();
                            });
                            });
</script>
@endsection
