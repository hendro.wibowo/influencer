@extends('layouts.dashboard')
@push('styles')
<link media="all" type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
<link media="all" type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link media="all" type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
<link media="all" type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">

<style>
table thead th {
    height: 1rem !important;
    color: #000000;
    background-color: #dddddd !important;
}

.dt-body-center {
    text-align: center;
}

.dt-body-right {
    text-align: right;
}
</style>
@endpush

@push('scripts')
{{ HTML::script('public/js/jquery.dataTables.min.js')}}
{{ HTML::script('public/js/dataTables.bootstrap4.min.js')}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script>
$(document).ready(function() {
    var backlinkTable = $('#backlinks_table').DataTable( {
        "processing": true,
        "serverSide": true,
        "paging": true,
        "searching": false,
        "ordering": true,
        "info": false,
        "ajax": {
            "url": "{{ route('gigs.influencers.ajax', ['isHome' => 0]) }}",
            "data": function(data) {
                var keyword = $('[name="keyword"]').val();
                var category = $('[name="category"]').val();
                var language = $('[name="language"]').val();

                data.filterKeyword = keyword;
                data.filterCategory = category;
                data.filterLanguage = language;
            }
        },
        "order": [[ 1, "asc" ]],
        "columns": [
            { "data": 'id', "name": 'id' },
            { "data": 'channel', "name": 'channel' },
            { "data": 'unique_visitor', "name": 'unique_visitor' },
            // { "data": 'followers', "name": 'followers' },
            // { "data": 'subscriber', "name": 'subscriber' },
            // { "data": 'likers', "name": 'likers' },
            // { "data": 'total_posts', "name": 'total_posts' },
            { "data": 'engagement_rate', "name": 'engagement_rate' },
            { "data": 'price', "name": 'price' },
            { "data": 'action', "name": 'action' },
        ],
        "columnDefs": [
            { "width": "5%", "targets": 0, "orderable": false },
            { "width": "8%", "targets": 2, "className": "dt-body-center" },
            { "width": "8%", "targets": 3, "className": "dt-body-center" },
            { "width": "8%", "targets": 4, "className": "dt-body-center" },
            // { "width": "8%", "targets": 5, "className": "dt-body-right" },
            { "width": "8%", "targets": 5, "className": "dt-body-center", "orderable": false }
        ]
    } );
    $('.category-select').select2({
        placeholder: 'Semua Kategori',
        theme: "bootstrap",
        width: '20rem'
    });
    $('.language-select').select2({
        placeholder: 'Semua Bahasa',
        theme: "bootstrap",
        width: '20rem'
    });
    $('[name="keyword"]').on('keyup', function(e){
        backlinkTable.draw();
    });
    $('[name="category"]').on('change', function(e){
        backlinkTable.draw();
    });
    $('[name="language"]').on('change', function(e){
        backlinkTable.draw();
    });
});
</script>
@endpush
@section('content')
<div class="main_dashboard" id="backtotop">
    @include('elements.topcategories')
    <section class="dashboard-section">
        <div class="container">
            <div class="row-listing">
                <div class="row">
                    <div class="col-12 mb-2" style="margin-bottom: 1rem;">
                        <form class="form-inline">
                            <div class="form-group">
                                <input type="text" name="keyword" class="form-control col-sm-6" id="keyword" placeholder="Cari channel" style="width:40rem">
                            </div>
                            <div class="form-group">
                                <select name="category" class="category-select">
                                    <option value=""></option>
                                    @foreach ($skills as $skill)
                                    <option value="{{ $skill->id }}">{{ $skill->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="language" class="language-select select2-single">
                                    <option value=""></option>
                                    @foreach ($languages as $language)
                                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                @include('homes.table')
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {

        $(".deltime").click('change', function (event) {
            updateresult ();
        });
        $(".deltimesub").click('change', function (event) {
            updateresult ();
        });
        $(".langg").click('change', function (event) {
            updateresult ();
        });
        $(document).on('click', '.ajaxpagee a', function () {
            var npage = $(this).html();
            if ($(this).html() == '»') {
                npage = $('.ajaxpagee .active').html() * 1 + 1;
            } else if ($(this).html() == '«') {
                npage = $('.ajaxpagee .active').html() * 1 - 1;
            }
            $('#pageidd').val(npage);
            updateresult ();
            return false;
        });

        $(".numbrreg").keypress(function (event) {
            if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
                event.preventDefault(); //stop character from entering input
            }
        });
    });
    function applyfilter(){
       // updateresult()
    }

    function updateresult(){
        var thisHref = $(location).attr('href');
        $.ajax({
            url: thisHref,
            type: "POST",
            data: $('#searchform').serialize(),
            beforeSend: function () { $("#searchloader").show();},
            complete: function () {$("#searchloader").hide();},
            success: function (result) {
               $('#loadgigs').html(result);
            }
        });
    }

    function clearfilter(){
        window.location.href = window.location.protocol;
    }

</script>

@endsection
