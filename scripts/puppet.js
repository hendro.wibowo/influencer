const axios = require('axios');
const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

(async () => {
  try {
    const args = require('minimist')(process.argv.slice(2));
    // const csrfToken = args.t;
    const account = args.u;
    const password = args.p;

    const targetUrl = 'https://accounts.google.com/signin/v2/identifier?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3Dhttps%253A%252F%252Fwww.youtube.com%252F&hl=en&ec=65620&flowName=GlifWebSignIn&flowEntry=ServiceLogin';
    const browser = await puppeteer.launch({headless: false}); // default is true
    const page = await browser.newPage();

    await page.goto(targetUrl, { waitUntil: 'networkidle2' });
    await page.waitFor(2000);
    await page.focus('input[type="email"]');
    // await page.keyboard.type('hendrothemail@gmail.com');
    await page.keyboard.type(account);
    await page.click('div[role="button"].U26fgb');
    await page.waitFor(3000);
    await page.focus('input[type="password"]');
    // await page.keyboard.type('lov3232114');
    await page.keyboard.type(password);
    await page.click('div#passwordNext');

    await page.waitForNavigation('https://www.youtube.com/');

    // await page.click('ytd-topbar-menu-button-renderer > button#avatar-btn');
    // await page.waitFor(3000);
    // // const avatarMenu = await page.$$('div.menu-container div.items a paper-item');
    // await page.$$eval('div.menu-container a', selectorMatched => {
    //   // console.log(selectorMatched);
    //   for(i in selectorMatched) {
      //     if(i == 0){
    //       selectorMatched[i].click();
    //       break;
    //     }
    //   }
    // });

    // await page.waitFor(5000);
    // await page.$$eval('div#tabsContent paper-tab', selectorMatched => {
      //   // console.log(selectorMatched);
    //   for(i in selectorMatched) {
      //     if(i == 5){
    //       selectorMatched[i].click();
    //       break;
    //     }
    //   }
    // });

    await page.goto('https://studio.youtube.com/', { waitUntil: 'load' });
    await page.waitFor(1000);
    await page.click('ytcd-channel-facts-item a.analytics-button');
    await page.waitForNavigation();
    await page.click('yta-time-picker div.ytcp-dropdown-trigger');
    await page.click('#text-item-4');

    const metrics = await page.$$eval('#metric-total', async elements => {
      const metrics = [];
      for(i in elements) {
        // if(i == 0){
          metrics.push(elements[i].textContext);
          // break;
        // }
      }

      return metrics;
    });

    axios.get('http://influencer.test/channel/save-metric', {
        data: metrics
      })
      .then(function (response) {
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });

    await page.waitFor(5000);
    await browser.close();
  } catch (error) {
    console.log(error);
  }
})();
